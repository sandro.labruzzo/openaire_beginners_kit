from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description="OpenAIRE Beginner's Kit.",
    author='Andrea Mannocc, Miriam Baglioni, Sandro La Bruzzo',
    license='MIT',
)
